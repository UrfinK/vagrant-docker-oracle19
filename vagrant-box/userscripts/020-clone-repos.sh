#!/bin/bash -x

SOURCE_ARCHIV=LINUX.X64_193000_db_home.zip

mkdir /vagrant/git -p
cd /vagrant/git
if [[ ! -f docker-images/OracleDatabase/SingleInstance/dockerfiles/19.3.0/Dockerfile ]] ;
    then
	git clone https://github.com/oracle/docker-images
fi

cd docker-images/OracleDatabase/SingleInstance/dockerfiles/19.3.0
if [[ ! -f 'LINUX.X64_193000_db_home.zip' ]] ;
    then
	wget http://172.22.1.65/oracle/$SOURCE_ARCHIV -O $SOURCE_ARCHIV
fi


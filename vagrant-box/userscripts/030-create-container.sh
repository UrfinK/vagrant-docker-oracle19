#!/bin/bash

cd /vagrant/git/docker-images/OracleDatabase/SingleInstance/dockerfiles

# Parameters:
#   -v: version to build
#       Choose one of: 11.2.0.2  12.1.0.2  12.2.0.1  18.3.0  18.4.0  19.3.0  21.3.0
#   -t: image_name:tag for the generated docker image
#   -e: creates image based on 'Enterprise Edition'
#   -s: creates image based on 'Standard Edition 2'
#   -x: creates image based on 'Express Edition'
#   -i: ignores the MD5 checksums
#   -o: passes on container build option
#
#* select one edition only: -e, -s, or -x

bash -x ./buildContainerImage.sh -v 19.3.0 -t test -s -o '--build-arg SLIMMING=false'

#docker run -t test -s \
#       -e ORACLE_PWD='operator'\
#       -e ORACLE_EDITION='standard'\
#       -o '--build-arg SLIMMING=false'
